<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Thêm sản phẩm</title>
</head>
<body>
 
    <form action="{{Route('save')}}" method="post">
    {{csrf_field()}}
        Tên sản phẩm: <br>
        <input type="text" name="ten_sp" id="ten_sp">
        <br>
        Số lượng: <br>
        <input type="number" name="so_luong" id="so_luong">
        <br>
        Nhà sản xuất: <br>
        <select name="id_categories" id="id_categories">
        <?php foreach ($nsx as $value) { ?>
                <option value="{{$value['id']}}">{{$value['name']}}</option>
        <?php } ?>
    
            
        </select>
        
        <br><br>
        <input type="submit" value="Thêm" id="submit">
    </form>
</body>
</html>