<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;
use App\categories;

class product_controller extends Controller
{
    public function hienthi() {
        $san_pham = product::all()->toArray();
        return view('product/hienthi')->with('san_pham',$san_pham);
    }

    public function them() {
        $nsx = categories::all()->toArray();
        return view('product/them')->with('nsx',$nsx);
    }

    public function sua($id) {
        $nsx = categories::all()->toArray();
        $sp = product::find($id)->toArray();
        return view('product/sua', compact('nsx', 'sp'));
    }

    public function xoa($id) {
        product::find($id)->delete();
        return redirect()->Route('index'); 
    }

    public function save(Request $rq) {
        $sp = new product();
        $sp->ten_sp = $rq->ten_sp;
        $sp->so_luong = $rq->so_luong;
        $sp->id_categories = $rq->id_categories;
        $sp->save();
        return redirect()->Route('index'); 
    }

    public function save2(Request $rq) {
        $sp = product::find($rq->id);
        $sp->ten_sp = $rq->ten_sp;
        $sp->so_luong = $rq->so_luong;
        $sp->id_categories = $rq->id_categories;
        $sp->save();
        return redirect()->Route('index');
    }

   
}
